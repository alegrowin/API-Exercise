﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TitanicApi.Models;

namespace TitanicApi.EFCore
{
    [Table("Passenger")]
    public class Passenger
    {
        [Required]
        public Guid PassengerID { get; set; }
        [Required]
        public bool Survived { get; set; }
        [Required]
        public int PassengerClass { get; set; }
        [Required]
        [StringLength(128)]
        public string Name { get; set; }
        [Required]
        public SexEnum Sex { get; set; }
        [Required]
        public float Age { get; set; }
        [Required]
        public int SiblingsOrSpousesAboard { get; set; }
        [Required]
        public int ParentsOrChildrenAboard { get; set; }
        [Required]
        public float Fare { get; set; }
    }
}
