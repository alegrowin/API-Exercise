﻿using TitanicApi.Models;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Linq;
using System;


namespace TitanicApi.EFCore
{
    public class TitanicDBContext : DbContext
    {
        public DbSet<Passenger> Passengers { get; set; }

        public TitanicDBContext(DbContextOptions<TitanicDBContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            int passengerID = 1;

            var passengers = File.ReadAllLines("titanic.csv")
                .Skip(1)
                .ToList()
                .Select(x => x.Split(','))
                .Select(x => new Passenger()
                {
                    PassengerID = ToGuid(passengerID++),
                    Survived = x[0].Equals("1"),
                    PassengerClass = Int32.Parse(x[1]),
                    Name = x[2],
                    Sex = (SexEnum)Enum.Parse(typeof(SexEnum), x[3], true),
                    Age = float.Parse(x[4]),
                    SiblingsOrSpousesAboard = Int32.Parse(x[5]),
                    ParentsOrChildrenAboard = Int32.Parse(x[6]),
                    Fare = float.Parse(x[7])
                });

            foreach (var passenger in passengers)
            {
                modelBuilder.Entity<Passenger>().HasData(passenger);
            }
        }

        public static Guid ToGuid(int value)
        {
            byte[] bytes = new byte[16];
            BitConverter.GetBytes(value).CopyTo(bytes, 0);
            return new Guid(bytes);
        }
    }
}