using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace TitanicApi.Models
{
    /// <summary>
    /// Gets or Sets Sex
    /// </summary>
    [JsonConverter(typeof(Newtonsoft.Json.Converters.StringEnumConverter))]
    public enum SexEnum
    {

        /// <summary>
        /// Enum MaleEnum for male
        /// </summary>
        [EnumMember(Value = "male")]
        Male = 1,

        /// <summary>
        /// Enum FemaleEnum for female
        /// </summary>
        [EnumMember(Value = "female")]
        Female = 2,

        /// <summary>
        /// Enum OtherEnum for other
        /// </summary>
        [EnumMember(Value = "other")]
        Other = 3
    }
}