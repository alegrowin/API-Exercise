#!/bin/bash

IMAGENAME=exercise/tools:latest
USER=$(whoami)

docker build ./tools --build-arg USER=$USER -t $IMAGENAME

unameOut="$(uname -s)"

if [[ "$unameOut" == "Linux" ]]
  then
    echo "kernel name is Linux, Setting XDG accordingly"
    XDG_CACHE_HOME=$HOME/.cache/helm
    XDG_CONFIG_HOME=$HOME/.config/helm
    XDG_DATA_HOME=$HOME/.local/share/helm
fi

if [[ "$unameOut" == "Darwin" ]]
  then
    echo "kernel name is Darwin, Setting XDG accordingly"
    XDG_CACHE_HOME=$HOME/Library/Caches/helm
    XDG_CONFIG_HOME=$HOME/Library/Preferences/helm
    XDG_DATA_HOME=$HOME/Library/helm
fi

function cli_alias() {
    docker run -it --rm \
        -v $(pwd):${1} \
        -v ${HOME}/.kube:/home/${USER}/.kube \
        -v ${HOME}/.minikube:${HOME}/.minikube \
        -v ${HOME}/.helm:/home/${USER}/.helm \
        -v ${XDG_CACHE_HOME}:/home/${USER}/.cache \
        -v ${XDG_CONFIG_HOME}:/home/${USER}/.config \
        -v ${XDG_DATA_HOME}:/home/${USER}/.local/share \
        --user 1000:1000 \
        --network host \
        -w $1 \
        $IMAGENAME "${@:2}"
}

# Kubernetes
alias kubectl="cli_alias /work kubectl"
# Helm
alias helm="cli_alias /work helm"

alias ssh_tools="cli_alias /work bash"
