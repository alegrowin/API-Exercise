#!/bin/bash
# sudo apt install virtualbox virtualbox-ext-pack
# wget https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
# chmod +x minikube-linux-amd64
# sudo mv minikube-linux-amd64 /usr/local/bin/minikube

minikube start --cpus 4 --memory 2048

eval $(minikube docker-env)
docker build . -t api
docker build UI/ -t ui

docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock -v "$(pwd)":"$(pwd)" -w "$(pwd)" -v "$HOME/.dive.yaml":"$HOME/.dive.yaml" -e CI=true wagoodman/dive:latest api
docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock -v "$(pwd)":"$(pwd)" -w "$(pwd)" -v "$HOME/.dive.yaml":"$HOME/.dive.yaml" -e CI=true wagoodman/dive:latest ui

unset DOCKER_TLS_VERIFY
unset DOCKER_HOST
unset DOCKER_CERT_PATH
unset MINIKUBE_ACTIVE_DOCKERD

source ./tools/aliases.sh

kubectl apply -f manifests/akrobateo

helm repo add containous-stable https://containous.github.io/traefik-helm-chart 
helm repo update
helm install --wait --set  image.tag="2.2" --set ports.websecure.expose="false" traefik containous-stable/traefik 
kubectl apply -f manifests/
kubectl apply -f manifests/metrics
kubectl apply -f manifests/user-mode

echo "Waiting for kubernetes to download mssql..."
kubectl wait --for=condition=available --timeout=600s -n default deployment/mssql
sleep 15
echo "Waiting for database migration and for the API to be ready..."
kubectl wait --for=condition=available --timeout=600s -n default deployment/api

IP=$(minikube ip)
echo "Minikube IP: $IP \n"

echo "Running tests Integration tests \n"
docker run -it --rm -v $(pwd):/data -w /data --network host postman/newman run --env-var url=titanic.$IP.xip.io ./TitanicAPI.postman_collection.json

echo "\nTraefik dashboard:"
echo http://traefik.$IP.xip.io/dashboard/ 
echo "\nSwagger:"
echo http://titanic.$IP.xip.io/swagger/index.html
echo "\nWebsite:"
echo http://titanic.$IP.xip.io/people
