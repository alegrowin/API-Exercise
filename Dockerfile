FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build
WORKDIR /src
COPY ["TitanicApi/TitanicApi.csproj", "TitanicApi/"]
RUN dotnet restore "TitanicApi/TitanicApi.csproj"
COPY . .
WORKDIR "/src/TitanicApi"
RUN dotnet build "TitanicApi.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "TitanicApi.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .

ENTRYPOINT ["dotnet", "TitanicApi.dll"]
