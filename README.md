# ![Titanic logo](https://img.icons8.com/color/48/000000/titanic.png "Architecture") Titanic Exercise

## About

This project is defined by the following [Tasks](Task.md):

1. [Create and Seed a Database](Task.md)
1. [Create an API](Task.md) following [a specification](API.md)
1. [Dockerize the API](Task.md)
1. [Deploy Inside Kubernetes](Task.md).
1. Extra milestone: create a [Simple UI](API.md)

## Requirements

In order to run this project either on Darwin or Linux, one should have these requirements:

- Install [Docker](https://docs.docker.com/engine/install/)
- port 80 MUST be available on host (Traefik is using it)
- Install [minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/) so it's available from the CLI.

### Optional Developer tools

- [Lens](https://github.com/lensapp/lens/releases)

## Architecture

![Architecture Diagram](doc/architecture.png "Architecture")

## Technologies

- [Akrobateo](https://github.com/kontena/akrobateo)
- [Traefik](https://containo.us/traefik/)
- [Kendo UI Core for jQuery](https://www.telerik.com/open)
- [ZURB Foundation](https://get.foundation/)
- [.Net Core 3.1](https://dotnet.microsoft.com/download/dotnet-core/3.1)
- [Entity Framework](https://docs.microsoft.com/en-us/ef/core/get-started/)
- [Swagger](https://swagger.io/)
- [Swashbuckle (.Net implementation)](https://github.com/domaindrivendev/Swashbuckle.AspNetCore)
- [MSSQL docker](https://hub.docker.com/_/microsoft-mssql-server)
- [minikube (local Kubernetes cluster)](https://github.com/kubernetes/minikube) 
- [docker](https://www.docker.com/)
- [postman](https://www.postman.com/)
- [newman (postman integration tests)](https://github.com/postmanlabs/newman)

## How to Run

```bash
# Source the script because of alias in ./tools/aliases.sh will not be recognized without this.
source ./install.sh
```

### Explanation of install.sh

1. Start Minikube
1. Build .Net Core API and NGinx static html docker images on minikube environment
1. Run Tests on newly created docker images
1. Configure CLI to use docker and create aliases for
    1. kubectl
    1. helm
1. Configure Cluster
    1. kubectl Akrobateo manifests
    1. helm Traefik chart
    1. kubectl Database
    1. kubectl API
    1. kubectl UI
    1. kubectl Lens addons
1. Wait for minikube to download mssql (1.50GB)
1. Wait for Database migration and for API to be ready
1. Run Postman integration Tests on minikube
1. Display Urls

## Demo

[![asciicast](https://asciinema.org/a/337343.png)](https://asciinema.org/a/337343)

### Available URLs

#### Traefik dashboard

http://traefik.172.17.0.3.xip.io/dashboard/

#### Swagger

http://titanic.172.17.0.3.xip.io/swagger/index.html

#### Website
http://titanic.172.17.0.3.xip.io/people

![Titanic Website](doc/web.png)


## Developer notes

### MSSQL

#### Connect to mssql server using mssql-tools

```bash
sudo kubectl run -it --rm mssql-tools --image mcr.microsoft.com/mssql-tools bash
```

##### Login to mssql instance

```bash
sqlcmd -S mssql -U sa -P "MyC0m9l&xP@ssw0rd"
```

### Entity Framework

Using core first migrations, API is in charge to create and maintain the database model. Ultimately if the Kubernetes deployment is rolling one at a time, this should be okay. However, in a larger production scenario where millions of transaction occurs in a relatively small amount of time. Database versioning and other data strategy need to be put in place to handle uptime and data resilience.

After modifying C# DBContext code, run the following to generate code first migration.

```bash
dotnet ef migrations add MigrationName
```

### Production SQL

Sometime, the developers are not responsible for maintaining the database and this may lead on creating SQL to execute migration and to bring optimization to production migration.

The following scripts let one create sql migration script using Entity Framework CLI.

```bash
dotnet ef migrations script 0 20200602212919_ChangedAgeFieldForFloatOnPassengerTable -o Sql_Scripts/V1_0_0_0__InitialMigration.sql --no-build
```

### Dive

Dive is an amazing tool to let you inspect your docker image and search for optimization.

```bash
# Build and inspect image
docker run --rm -it \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -v  "$(pwd)":"$(pwd)" \
      -w "$(pwd)" \
      -v "$HOME/.dive.yaml":"$HOME/.dive.yaml" \
      wagoodman/dive:latest build -t tools .

# Validate image footprint in CI
docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock -v "$(pwd)":"$(pwd)" -w "$(pwd)" -v "$HOME/.dive.yaml":"$HOME/.dive.yaml" -e CI=true wagoodman/dive:latest tools
```

### Swagger | openAPI

```bash
docker run --rm -v ${PWD}:/local swaggerapi/swagger-codegen-cli generate \
    -i ./swagger.yml \
    -l aspnetcore \
    -o /local/generated
```

## Additional Notes

I originally thought it was a good idea to create the application using k3s as I was encouraged to get out of the comfort zone and try new things. Not the first time I realize that going all in with Linux sometime isn't the perfect solution as OSx as often his own way to do thing.

### Learning

Before this project I never used [k3s](https://k3s.io/), [minikube](https://kubernetes.io/docs/setup/learning-environment/minikube/) and [traefik](https://containo.us/traefik/). In the past, I used kubernetes in docker, AKS, EKS, and GKE and multiple service mesh like Istio and Ambassador.

I also experimented with [Evolve](https://evolve-db.netlify.app/), a database migration tool for .NET and .NET Core, inspired by Flyway. Turn out this could be a good approach for more solid database migrations.

### Other extra miles

Time is precious, but I would have loved to implement more security on docker images, create an helm chart and even terraform some infrastructure on cloud where I have an expertise. Maybe I will have more opportunity to share and learn with you in the future! :D

#### Learning never ends 

![Alt Text](https://media.giphy.com/media/26ybw6AltpBRmyS76/giphy.gif)
